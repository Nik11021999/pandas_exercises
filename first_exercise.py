# First exercise with pandas : Given the Dataset file "IoT23_obfuscated", calculate :
#   - empirical cdf of the packet length values for each bi-flow ;
#   - bar plot of the number of bi-flow for each class (based on the label value) .

import pyarrow as pa
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter
import pandas as pd

# Read Data from file
df = pd.read_parquet('dataset_20p_6f_576b_no_obf_TOY.parquet')

## Bar plot
# Matplotlib
'''x = df["LABEL"].values
Dict_count = Counter(x)
plt.barh(range(len(Dict_count)), list(Dict_count.values()))
plt.yticks(range(len(Dict_count)), list(Dict_count.keys()))
plt.xlabel("Bi-flow Class")
plt.ylabel("Frequency")
plt.show()'''

# Pandas only
df['LABEL'].value_counts().plot(kind='barh')
plt.xlabel("Bi-flow Class")
plt.ylabel("Frequency")
plt.show()


## Empirical CDF
classes = df['LABEL'].drop_duplicates().values   #List of different bi-flow classes
all_cdf = {}   #Dictionary with cdf values for each class
all_pl_values = {}
for x in classes:
    length_eval = []    #Array containing the packet lengths for which the cdf value has been already computed
                        #(this array will be cleared before the analysis of each class)
    class_cdf = {}      #Dictionary containing the packet lengths (of the current class) as keys and the corresponding
                        #cdf value as values (this dictionary will be cleared before the analysis of each class)

    tmp = df.loc[df['LABEL'] == x]  #List of arrays with the packet lengths of all the bi-flow with the same label
    pl_raw = np.concatenate(tmp['PL'].values, axis=0)    #Transform the list of arrays into a single array
    pl_values = [p for p in pl_raw if p != 0]
    all_pl_values[x] = pl_values

    #Given a class, for each different packet length, the corresponding cdf value will be computed.
    for pl in pl_values:
        if pl not in length_eval:
            length_eval.append(pl)
            inf_count = len([x for x in pl_values if x <= pl])
            cdf_value = inf_count/len(pl_values)
            class_cdf[pl] = cdf_value   #Add the value to the dictionary

    # Sorting the cdf dictionaries by value
    tmp = {k: v for k, v in sorted(class_cdf.items(), key=lambda item: item[1])}

    all_cdf[x] = tmp  # Append the dictionary containing the cdf information of the class 'x'
                      # to the dictionary containing the cdf information of all classes


# Plot the CDF of each class
for k in all_cdf.keys():
    plt.figure(k)
    plt.plot(all_cdf[k].keys(), all_cdf[k].values())
    plt.grid()

    plt.figure(k + ' - plt.hist function')
    plt.hist(all_pl_values[k], density=True, cumulative=True, histtype='step')
    plt.grid()
    plt.show()